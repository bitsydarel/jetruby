package com.dbeginc.jetruby.di.modules

import com.dbeginc.jetruby.data.ShotsRepository
import dagger.Module
import dagger.Provides
import org.mockito.Mockito
import javax.inject.Singleton

/**
 * Created by darel on 12.07.17.
 * Provide all the test data
 * Needed to run tests
 */
@Module class TestDataModule {


    @Provides
    @Singleton
    internal fun provideShotsRepo() : ShotsRepository = Mockito.mock(ShotsRepository::class.java)

}