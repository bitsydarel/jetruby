package com.dbeginc.jetruby.di.modules

import com.dbeginc.jetruby.di.qualifiers.ComputationThread
import com.dbeginc.jetruby.di.qualifiers.NetworkThread
import com.dbeginc.jetruby.di.qualifiers.UiThread
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import javax.inject.Singleton

/**
 * Created by darel on 12.07.17.
 * This Module Provide Schedulers
 * that the Asynchronous tests need
 */
@Module class TestThreadModule {
    @Singleton
    @NetworkThread
    @Provides
    internal fun provideNetworkThread() : Scheduler = Schedulers.trampoline()

    @Singleton
    @UiThread
    @Provides
    internal fun provideUiThread() : Scheduler = Schedulers.trampoline()

    @Singleton
    @ComputationThread
    @Provides
    internal fun provideComputationThread() : Scheduler = Schedulers.trampoline()
}