package com.dbeginc.jetruby.di.components

import com.dbeginc.jetruby.di.modules.TestDataModule
import com.dbeginc.jetruby.di.modules.TestThreadModule
import com.dbeginc.jetruby.timeline.presenter.TimeLinePresenterTest
import dagger.Component
import javax.inject.Singleton

/**
 * Created by darel on 12.07.17.
 * Test Component
 */
@Singleton
@Component(modules = arrayOf(TestDataModule::class, TestThreadModule::class))
interface TestComponent {
    fun inject(timeLinePresenterTest: TimeLinePresenterTest)
}