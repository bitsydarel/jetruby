package com.dbeginc.jetruby.timeline.presenter

import com.dbeginc.jetruby.ConstantHolder
import com.dbeginc.jetruby.data.ShotsRepository
import com.dbeginc.jetruby.di.components.DaggerTestComponent
import com.dbeginc.jetruby.di.qualifiers.NetworkThread
import com.dbeginc.jetruby.di.qualifiers.UiThread
import com.dbeginc.jetruby.domain.entities.Images
import com.dbeginc.jetruby.domain.entities.Shot
import com.dbeginc.jetruby.timeline.views.ITimeLineView
import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.subscribers.DisposableSubscriber
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import javax.inject.Inject

/**
 * Created by darel on 12.07.17.
 * Unit Test Of the TimelinePresenter
 */
@RunWith(MockitoJUnitRunner::class)
class TimeLinePresenterTest {

    val EMPTY_LIST_OF_SHOTS = emptyList<Shot>()
    val LIST_OF_SHOTS = listOf(Shot(1245L, "Testing is awesome", "some desc", Images(null, null, null),false),
            Shot(1245L, "Testing is awesome", "some desc", Images(null, null, null),true))

    @Mock
    lateinit var view: ITimeLineView

    @Inject
    lateinit var shotsRepo: ShotsRepository

    @Inject @field:UiThread
    lateinit var mainThread: Scheduler

    @Inject @field:NetworkThread
    lateinit var ioThread: Scheduler

    lateinit var presenter: TimeLinePresenter
    val observer = ShotsObserver()

    inner class ShotsObserver : DisposableSubscriber<List<Shot>>() {
        override fun onNext(t: List<Shot>) = view.showTimeLine(t)
        override fun onComplete() = this.dispose()
        override fun onError(t: Throwable) = view.onTimeLineUpdateError()
    }

    @Before
    fun setUp() {
        DaggerTestComponent.create().inject(this)
        presenter = TimeLinePresenter(shotsRepo, mainThread)
    }

    /**
     * Test if view successfully received data
     * then displayed it
     */
    @Test
    fun shouldReceivedShots() {
        Mockito.`when`(shotsRepo.getShots(ConstantHolder.AUTH_TOKEN_FOR_TEST))
                .thenReturn(Flowable.just(LIST_OF_SHOTS).subscribeOn(ioThread))

        presenter.fetchShots(observer, ConstantHolder.AUTH_TOKEN_FOR_TEST)
                .observeOn(mainThread)

        Mockito.verify(view).showTimeLine(LIST_OF_SHOTS)
    }

    /**
     * Test if view handled error while fetching the shots
     */
    @Test
    fun shouldHandleError() {
        Mockito.`when`(shotsRepo.getShots(ConstantHolder.AUTH_TOKEN_FOR_TEST)).thenReturn(
            Flowable.just(LIST_OF_SHOTS)
                    .filter { it[4].animated // array index out of bounds exception
                        }
        )
        presenter.fetchShots(observer, ConstantHolder.AUTH_TOKEN_FOR_TEST).observeOn(mainThread)
        Mockito.verify(view).onTimeLineUpdateError()
    }

    /**
     * test if the presenter successfully setup the view
     */
    @Test
    fun shouldSetupView() {
        presenter.bind(view)
        Mockito.verify(view).setupShotsContainer()
    }

    /**
     * Test if presenter successfully dispose the subscription
     * when unbinding himselft from the view
     */
    @Test
    fun shouldReleaseResources() {
        //Return a flowable that never finish
        Mockito.`when`(shotsRepo.getShots(ConstantHolder.AUTH_TOKEN_FOR_TEST))
                .thenReturn(Flowable.never())

        presenter.fetchShots(observer, ConstantHolder.AUTH_TOKEN_FOR_TEST)
                .observeOn(mainThread)

        //Check if observer is still observing
        Assert.assertEquals(false, observer.isDisposed)

        //Release resource
        presenter.unbind()
        Assert.assertEquals(true, observer.isDisposed)
    }
}