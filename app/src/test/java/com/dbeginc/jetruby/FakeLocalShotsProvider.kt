package com.dbeginc.jetruby

import com.dbeginc.jetruby.data.IShotProvider
import com.dbeginc.jetruby.domain.entities.Shot
import io.reactivex.Flowable

/**
 * Created by darel on 12.07.17.
 */
class FakeLocalShotsProvider : IShotProvider<String, Flowable<List<Shot>>>  {
    lateinit var data: List<Shot>
    override fun getShots(param: String): Flowable<List<Shot>> = Flowable.just(data)
}