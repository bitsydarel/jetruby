package com.dbeginc.jetruby.di.qualifiers

import javax.inject.Qualifier

/**
 * Created by darel on 11.07.17.
 * This File has all Thread related Qualifiers
 */

/**
 * Qualifiers for Network Thread
 */
@Qualifier annotation class NetworkThread

/**
 * Qualifiers for User Interface Thread
 */
@Qualifier annotation class UiThread

/**
 * Qualifiers for Computation Thread
 */
@Qualifier annotation class ComputationThread
