package com.dbeginc.jetruby.di.components

import com.dbeginc.jetruby.authentication.view.AuthenticationFragment
import com.dbeginc.jetruby.di.modules.AppModule
import com.dbeginc.jetruby.di.modules.NetworkModule
import com.dbeginc.jetruby.di.modules.PresentationModule
import com.dbeginc.jetruby.di.modules.ThreadModule
import com.dbeginc.jetruby.base.BaseActivity
import com.dbeginc.jetruby.base.BaseFragment
import com.dbeginc.jetruby.home.views.HomeActivity
import com.dbeginc.jetruby.timeline.views.TimeLineFragment
import com.dbeginc.jetruby.utils.GlideConfiguration
import dagger.Component
import javax.inject.Singleton

/**
 * Created by darel on 10.07.17.
 * Application Component
 */
@Singleton
@Component(modules = arrayOf(AppModule::class, PresentationModule::class, NetworkModule::class, ThreadModule::class))
interface JetRubyAppComponent {

    fun inject(baseActivity: BaseActivity)
    fun inject(mainActivity: HomeActivity)
    fun inject(mainActivity: GlideConfiguration)
    fun inject(baseFragment: BaseFragment)
    fun inject(authFragment: AuthenticationFragment)
    fun inject(timelineFragment: TimeLineFragment)
}