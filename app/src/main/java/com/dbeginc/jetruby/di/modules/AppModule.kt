package com.dbeginc.jetruby.di.modules

import android.arch.persistence.room.Room
import android.content.Context
import android.content.res.Resources
import com.dbeginc.jetruby.JetRubyApp
import com.dbeginc.jetruby.data.AppDatabase
import com.dbeginc.jetruby.data.IShotProvider
import com.dbeginc.jetruby.data.ShotsRepository
import com.dbeginc.jetruby.data.authentification.DribbleAuthService
import com.dbeginc.jetruby.data.remote.RemoteShotProvider
import com.dbeginc.jetruby.di.qualifiers.AuthenticationEvent
import com.dbeginc.jetruby.di.qualifiers.LocalShots
import com.dbeginc.jetruby.di.qualifiers.NetworkThread
import com.dbeginc.jetruby.di.qualifiers.RemoteShots
import com.dbeginc.jetruby.domain.entities.Shot
import com.dbeginc.jetruby.utils.ConstantHolder
import dagger.Module
import dagger.Provides
import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.subjects.PublishSubject
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by darel on 10.07.17.
 * Application Module
 *
 * PS: Using Singleton Scope not custom scope for some instances
 * because it's only one activity application
 */
@Module class AppModule(val application: JetRubyApp) {

    @Provides
    @Singleton
    internal fun provideAppContext() = application.applicationContext

    @Provides
    @Singleton
    @AuthenticationEvent
    internal fun provideAuthEvents() : PublishSubject<Boolean> = PublishSubject.create()

    @Provides
    @Singleton
    internal fun provideResources() : Resources = application.resources

    @Provides
    @Singleton
    internal fun provideSharedPrefs(context: Context) = context.getSharedPreferences(ConstantHolder.SHARED_PREF_NAME, Context.MODE_PRIVATE)

    @Provides
    @Singleton
    internal fun provideAuthService() : DribbleAuthService = DribbleAuthService()

    @Provides
    @Singleton
    internal fun provideDatabase(context: Context) : AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, "jetruby_db")
                .build()
    }

    @Provides
    @Singleton
    @LocalShots
    internal fun provideLocaleShotsTable(appDatabase: AppDatabase) : IShotProvider<String, Flowable<List<Shot>>> {
        return appDatabase.getShotsTable()
    }

    @Provides
    @Singleton
    @RemoteShots
    internal fun provideRemoteShotsTable(retrofit: Retrofit) : IShotProvider<String, Flowable<List<Shot>>> {
        return RemoteShotProvider(retrofit)
    }

    @Provides
    @Singleton
    internal fun provideShotsRepo(@LocalShots localProvider: @JvmSuppressWildcards IShotProvider<String, Flowable<List<Shot>>>,
                                  @RemoteShots remoteProvider: @JvmSuppressWildcards IShotProvider<String, Flowable<List<Shot>>>,
                                  @NetworkThread ioThread: Scheduler) : ShotsRepository {

        return ShotsRepository(localProvider, remoteProvider, ioThread)
    }
}