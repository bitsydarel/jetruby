package com.dbeginc.jetruby.di.modules

import com.dbeginc.jetruby.authentication.presenter.AuthPresenter
import com.dbeginc.jetruby.data.ShotsRepository
import com.dbeginc.jetruby.data.authentification.DribbleAuthService
import com.dbeginc.jetruby.di.qualifiers.UiThread
import com.dbeginc.jetruby.home.presenter.HomePresenter
import com.dbeginc.jetruby.timeline.presenter.TimeLinePresenter
import com.dbeginc.jetruby.utils.Navigator
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import javax.inject.Singleton

/**
 * Created by darel on 10.07.17.
 * Presentation Module
 */
@Module
class PresentationModule {

    @Provides
    internal fun provideHomePresenter() : HomePresenter = HomePresenter()

    @Provides
    internal fun provideShotsPresenter(shotsRepository: ShotsRepository, @UiThread mainThread: Scheduler) : TimeLinePresenter {
        return TimeLinePresenter(shotsRepository, mainThread)
    }

    @Provides
    internal fun provideAuthPresenter(authService: DribbleAuthService) : AuthPresenter = AuthPresenter(authService)

    @Provides
    @Singleton
    internal fun provideNavigator() : Navigator = Navigator()
}