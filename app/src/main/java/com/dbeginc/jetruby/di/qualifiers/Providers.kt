package com.dbeginc.jetruby.di.qualifiers

import javax.inject.Qualifier

/**
 * Created by darel on 11.07.17.
 * This File has all data related provider
 */

/**
 * Qualifiers for Local Shots Provider
 */
@Qualifier annotation class LocalShots

/**
 * Qualifiers for Remote Shots Provider
 */
@Qualifier annotation class RemoteShots

/**
 * Qualifiers for Authentication Events
 */
@Qualifier annotation class AuthenticationEvent
