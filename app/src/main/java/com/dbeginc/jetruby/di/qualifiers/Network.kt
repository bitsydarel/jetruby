package com.dbeginc.jetruby.di.qualifiers

import javax.inject.Qualifier

/**
 * Created by darel on 11.07.17.
 * This File has all the network related Qualifiers
 */

/**
 * Qualifiers for Network cache
 */
@Qualifier annotation class NetworkCache

/**
 * Qualifiers for Image cache
 */
@Qualifier annotation class ImageCache

/**
 * Qualifiers for Image Downloader
 */
@Qualifier annotation class ImageDownloader

/**
 * Qualifiers for Image Downloader
 */
@Qualifier annotation class ContentDownloader
