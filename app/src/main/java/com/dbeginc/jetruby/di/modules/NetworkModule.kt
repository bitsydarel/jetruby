package com.dbeginc.jetruby.di.modules

import android.content.Context
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory
import com.dbeginc.jetruby.di.qualifiers.ContentDownloader
import com.dbeginc.jetruby.di.qualifiers.ImageCache
import com.dbeginc.jetruby.di.qualifiers.ImageDownloader
import com.dbeginc.jetruby.di.qualifiers.NetworkCache
import com.dbeginc.jetruby.utils.ConstantHolder
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Created by darel on 10.07.17.
 * Network Module
 */
@Module class NetworkModule {

    @Provides
    @Singleton
    @NetworkCache
    internal fun provideNetworkCache(context: Context): Cache = Cache(File(context.cacheDir, ConstantHolder.APP_CACHE_NAME), ConstantHolder.CACHE_SIZE)

    @Provides
    @Singleton
    @ImageCache
    internal fun providesImageCache(context: Context): InternalCacheDiskCacheFactory {
        return InternalCacheDiskCacheFactory(context, ConstantHolder.GlIDE_CACHE_NAME, ConstantHolder.CACHE_SIZE.toInt())
    }

    @Provides
    @Singleton
    @ImageDownloader
    internal fun providesImageDownloader(@NetworkCache cache: Cache) : OkHttpClient = OkHttpClient.Builder()
            .connectTimeout(35, TimeUnit.SECONDS)
            .writeTimeout(35, TimeUnit.SECONDS)
            .readTimeout(55, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
            .cache(cache)
            .build()

    @Provides
    @Singleton
    @ContentDownloader
    internal fun provideShotsDownloader(@NetworkCache cache: Cache) : OkHttpClient = OkHttpClient.Builder()
            .connectTimeout(35, TimeUnit.SECONDS)
            .writeTimeout(35, TimeUnit.SECONDS)
            .readTimeout(55, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
            .cache(cache)
            .build()

    @Provides
    @Singleton
    internal fun provideRetrofitShot(@ContentDownloader client: OkHttpClient) : Retrofit = Retrofit.Builder()
            .client(client)
            .baseUrl(ConstantHolder.ShOTS_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

}