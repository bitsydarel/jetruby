package com.dbeginc.jetruby.di.modules

import com.dbeginc.jetruby.di.qualifiers.ComputationThread
import com.dbeginc.jetruby.di.qualifiers.NetworkThread
import com.dbeginc.jetruby.di.qualifiers.UiThread
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Singleton

/**
 * Created by darel on 10.07.17.
 * This Module Provide Schedulers
 * that the application need
 */
@Module class ThreadModule {
    @Singleton
    @NetworkThread
    @Provides
    internal fun provideNetworkThread() : Scheduler = Schedulers.io()

    @Singleton
    @UiThread
    @Provides
    internal fun provideUiThread() : Scheduler = AndroidSchedulers.mainThread()

    @Singleton
    @ComputationThread
    @Provides
    internal fun provideComputationThread() : Scheduler = Schedulers.computation()

}