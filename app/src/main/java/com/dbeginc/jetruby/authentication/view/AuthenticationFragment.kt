package com.dbeginc.jetruby.authentication.view

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.annotation.UiThread
import android.support.design.widget.Snackbar
import android.support.v4.content.res.ResourcesCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.agilie.dribbblesdk.service.auth.AuthCredentials
import com.agilie.dribbblesdk.service.auth.DribbbleAuthHelper
import com.dbeginc.jetruby.BuildConfig
import com.dbeginc.jetruby.JetRubyApp
import com.dbeginc.jetruby.R
import com.dbeginc.jetruby.authentication.presenter.AuthPresenter
import com.dbeginc.jetruby.databinding.AuthenticationFragmentBinding
import com.dbeginc.jetruby.base.BaseFragment
import com.dbeginc.jetruby.di.qualifiers.AuthenticationEvent
import com.dbeginc.jetruby.utils.ConstantHolder
import com.google.api.client.auth.oauth2.Credential
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.subjects.PublishSubject
import java.lang.Exception
import javax.inject.Inject

/**
 * Created by darel on 10.07.17.
 * Authentication View Implementation
 */
class AuthenticationFragment : BaseFragment(), IAuthenticationView {

    @Inject lateinit var presenter: AuthPresenter
    @Inject @field:AuthenticationEvent
    lateinit var authEvent:PublishSubject<Boolean>

    private val redirectUrl = "https://jetruby.com/"
    private lateinit var binding: AuthenticationFragmentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        JetRubyApp.graph.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.authentication_fragment, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.bind(this)
    }

    override fun login() {
        val cred = AuthCredentials
                .newBuilder(BuildConfig.DribbbleClientId, BuildConfig.DribbbleClientSecret, BuildConfig.DribbbleClientAccessToken, redirectUrl)
                .build()

        val listener = object : DribbbleAuthHelper.AuthListener {
            override fun onSuccess(credential: Credential) {
                onAuthenticationSuccess(credential.accessToken)
            }

            override fun onError(exception: Exception) = onAuthenticationFailed(exception.localizedMessage)
        }
        presenter.authenticate(cred, Observer(), listener)
    }

    @UiThread
    override fun onAuthenticationSuccess(token: String) {
        sharedPref.edit()
                .putString(ConstantHolder.AUTH_TOKEN, token)
                .apply()

        sharedPref.edit()
                .putBoolean(ConstantHolder.IS_FIRST_RUN, false)
                .apply()

        if (isVisible) {
            Snackbar.make(binding.authContainer, "Authentication Successfull", Snackbar.LENGTH_LONG).show()
        }
        authEvent.onNext(true)
    }

    @UiThread
    override fun onAuthenticationFailed(exception: String) {
        if (isVisible) {
            Snackbar.make(binding.authContainer, "$exception, please retry", Snackbar.LENGTH_INDEFINITE)
                    .setAction("retry", { login() })
                    .setActionTextColor(ResourcesCompat.getColor(resource, android.R.color.holo_red_dark, activity.theme))
                    .show()
        }
        authEvent.onNext(false)
    }

    inner class Observer: DisposableCompletableObserver() {
        override fun onComplete() = dispose()
        override fun onError(e: Throwable) = onAuthenticationFailed(e.localizedMessage)
    }
}