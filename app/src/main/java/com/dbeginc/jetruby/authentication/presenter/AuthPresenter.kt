package com.dbeginc.jetruby.authentication.presenter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import com.agilie.dribbblesdk.service.auth.AuthCredentials
import com.agilie.dribbblesdk.service.auth.DribbbleAuthHelper
import com.dbeginc.jetruby.authentication.view.IAuthenticationView
import com.dbeginc.jetruby.data.authentification.IAuthService
import com.dbeginc.jetruby.domain.usecases.Authentication
import com.dbeginc.jetruby.domain.entities.AsyncParams
import com.dbeginc.jetruby.base.IPresenter
import io.reactivex.observers.DisposableCompletableObserver

/**
 * Created by darel on 10.07.17.
 * Authentication View Presenter
 * He control the behavior of the authentication View
 */
class AuthPresenter(authService: IAuthService<AuthCredentials, Pair<FragmentActivity, DribbbleAuthHelper.AuthListener>>) : IPresenter<IAuthenticationView> {

    private lateinit var view : IAuthenticationView
    private val authentication = Authentication(authService)

    fun authenticate(credentials: AuthCredentials, observerInfo: DisposableCompletableObserver, observer: DribbbleAuthHelper.AuthListener) {
        authentication.execute(observerInfo, AsyncParams(credentials, (view as Fragment).activity to observer))
    }

    override fun bind(newView: IAuthenticationView) {
        this.view = newView
        this.view.login()
    }

    override fun unbind() = authentication.dispose()
}