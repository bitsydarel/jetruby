package com.dbeginc.jetruby.authentication.view

/**
 * Created by darel on 10.07.17.
 * Representation of an authentication View
 */
interface IAuthenticationView {

    /**
     * Authenticate the user
     */
    fun login()

    /**
     * Notify if the authentication was successful
     * @param token user token
     */
    fun onAuthenticationSuccess(token: String)

    /**
     * Notify if the authentication was successful
     * @param exception failure cause of the authentication
     */
    fun onAuthenticationFailed(exception: String)
}