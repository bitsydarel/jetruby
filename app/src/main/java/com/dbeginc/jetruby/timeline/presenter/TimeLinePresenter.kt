package com.dbeginc.jetruby.timeline.presenter

import com.dbeginc.jetruby.base.IPresenter
import com.dbeginc.jetruby.data.ShotsRepository
import com.dbeginc.jetruby.domain.usecases.GetShots
import com.dbeginc.jetruby.domain.entities.AsyncParams
import com.dbeginc.jetruby.domain.entities.Shot
import com.dbeginc.jetruby.timeline.views.ITimeLineView
import io.reactivex.Scheduler
import io.reactivex.subscribers.DisposableSubscriber

/**
 * Created by darel on 10.07.17.
 * TimeLine Presenter
 */
class TimeLinePresenter(shotsRepository: ShotsRepository, private val mainThread: Scheduler) : IPresenter<ITimeLineView> {

    private val getShots = GetShots(shotsRepository)
    private lateinit var view : ITimeLineView

    fun fetchShots(observer: DisposableSubscriber<List<Shot>>, token: String) = getShots.execute(observer, AsyncParams(token, Unit))
            .observeOn(mainThread)

    override fun bind(newView: ITimeLineView) {
        this.view = newView
        this.view.setupShotsContainer()
    }

    override fun unbind() = getShots.dispose()
}