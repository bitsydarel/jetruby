package com.dbeginc.jetruby.timeline.views

import com.dbeginc.jetruby.domain.entities.Shot

/**
 * Created by darel on 10.07.17.
 * Representation of a timeLine View
 */
interface ITimeLineView {

    /**
     * Show timeline of shots
     * @param listOfShot
     */
    fun showTimeLine(listOfShot: List<Shot>)

    /**
     * Setup shots container
     */
    fun setupShotsContainer()

    /**
     * Update timeline shots
     */
    fun updateTimeLine()

    /**
     * On Timeline update error
     */
    fun onTimeLineUpdateError()
}