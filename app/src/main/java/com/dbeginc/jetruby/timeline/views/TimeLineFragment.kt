package com.dbeginc.jetruby.timeline.views

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.annotation.MainThread
import android.support.design.widget.Snackbar
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dbeginc.jetruby.JetRubyApp
import com.dbeginc.jetruby.R
import com.dbeginc.jetruby.base.BaseFragment
import com.dbeginc.jetruby.databinding.TimelineFragmentBinding
import com.dbeginc.jetruby.domain.entities.Shot
import com.dbeginc.jetruby.timeline.adapter.ShotsAdapter
import com.dbeginc.jetruby.timeline.presenter.TimeLinePresenter
import com.dbeginc.jetruby.utils.ConstantHolder
import io.reactivex.subscribers.DisposableSubscriber
import javax.inject.Inject

/**
 * Created by darel on 10.07.17.
 * TimeLine View Impl
 */
class TimeLineFragment : BaseFragment(), ITimeLineView {

    @Inject lateinit var presenter: TimeLinePresenter
    private lateinit var binding: TimelineFragmentBinding
    private lateinit var token: String
    private lateinit var adapter: ShotsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        JetRubyApp.graph.inject(this)

        val shots : List<Shot>? = savedInstanceState?.getParcelableArrayList(ConstantHolder.SHOTS_KEY)
        if (shots != null) adapter = ShotsAdapter(shots)
        else adapter = ShotsAdapter(emptyList())

        token = sharedPref.getString(ConstantHolder.AUTH_TOKEN,"")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.timeline_fragment, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.bind(this)
        binding.swipeRefresh.isRefreshing = true
        updateTimeLine()
    }

    override fun onDestroyView() {
        presenter.unbind()
        super.onDestroyView()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelableArrayList(ConstantHolder.SHOTS_KEY, ArrayList(adapter.shots))
    }

    override fun setupShotsContainer() {
        binding.recvShots.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.recvShots.setHasFixedSize(true)
        binding.recvShots.post { binding.recvShots.adapter = adapter }
        binding.swipeRefresh.setOnRefreshListener { updateTimeLine() }
    }

    @MainThread
    override fun showTimeLine(listOfShot: List<Shot>) {
        adapter.update(listOfShot)
        activity?.runOnUiThread { binding.swipeRefresh.isRefreshing = false }
    }

    override fun onTimeLineUpdateError() {
        if (isVisible) {
            Snackbar.make(binding.swipeRefresh, "Error while getting shots", Snackbar.LENGTH_LONG)
                    .setAction("retry", { updateTimeLine() })
                    .setActionTextColor(ResourcesCompat.getColor(resource, android.R.color.holo_red_dark, activity.theme))
                    .show()
        }
    }

    override fun updateTimeLine() { presenter.fetchShots(ShotsObserver(), token) }

    inner class ShotsObserver : DisposableSubscriber<List<Shot>>() {
        override fun onNext(listOfShot: List<Shot>) {
            return if (listOfShot.isEmpty()) onTimeLineUpdateError()
                   else showTimeLine(listOfShot)
        }
        override fun onError(t: Throwable) = onTimeLineUpdateError()
        override fun onComplete() = dispose()
    }
}