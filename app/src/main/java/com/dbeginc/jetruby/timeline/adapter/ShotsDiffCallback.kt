package com.dbeginc.jetruby.timeline.adapter

import android.support.v7.util.DiffUtil
import com.dbeginc.jetruby.domain.entities.Shot

/**
 * Created by darel on 11.07.17.
 * Time Line DiffCallback
 * Calculate
 */
class ShotsDiffCallback(val oldList: List<Shot>, val newList: List<Shot>) : DiffUtil.Callback() {
    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean = oldList[oldItemPosition].id == newList[newItemPosition].id

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean = oldList[oldItemPosition] == newList[newItemPosition]
}