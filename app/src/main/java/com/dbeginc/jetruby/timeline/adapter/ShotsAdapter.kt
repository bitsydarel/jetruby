package com.dbeginc.jetruby.timeline.adapter

import android.databinding.DataBindingUtil
import android.support.annotation.MainThread
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.dbeginc.jetruby.R
import com.dbeginc.jetruby.databinding.TimelineItemsBinding
import com.dbeginc.jetruby.domain.entities.Shot

/**
 * Created by darel on 11.07.17.
 * Shots Adapter
 */
class ShotsAdapter(shots: List<Shot>) : RecyclerView.Adapter<ShotsAdapter.ShotViewHolder>() {

    private var container: RecyclerView? = null
    var shots: List<Shot> = shots
        private set

    override fun getItemCount(): Int = shots.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShotViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ShotViewHolder(
                DataBindingUtil.inflate(inflater, R.layout.timeline_items, parent, false)
        )
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        container = recyclerView
    }

    override fun onBindViewHolder(holder: ShotViewHolder?, position: Int) {
        holder?.bindShot(shots[position])
    }

    @MainThread
    fun update(newShots: List<Shot>) {

        val diffResult = DiffUtil.calculateDiff(ShotsDiffCallback(shots, newShots))
        shots = newShots
        // Posting diffUtils result on the recyclerView queue
        // Which fix the Illegal statement Layout computing
        // And updating the layout changes on the container queue
        container?.post { diffResult.dispatchUpdatesTo(this) }
    }

    inner class ShotViewHolder(val binding: TimelineItemsBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindShot(shot: Shot) {
            binding.shot = shot
            binding.executePendingBindings()
        }
    }
}
