package com.dbeginc.jetruby.domain.usecases

import io.reactivex.Completable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableCompletableObserver

/**
 * Created by darel on 10.07.17.
 * Abstract class for a Use Case (Interactor in terms of Clean Architecture).
 * This interface represents a execution unit for different use cases which only notify about completion
 *
 * By convention each UseCase that implementation will return no result using a {@link DisposableCompletableObserver}
 * that will execute its job in a background thread and will post the result in the UI thread.
 */
abstract class UseCaseCompletable<Params> {
    private val subscriptions = CompositeDisposable()

    /**
     * Builds an [Completable] which will be used when executing the current [UseCaseCompletable].
     */
    internal abstract fun buildUseCaseCompletableObservable(params: Params): Completable

    /**
     * Executes the current use case.
     * @param observer [DisposableCompletableObserver] which will be listening to the observable build
     * by [.buildUseCaseCompletableObservable] ()} method.
     *
     * @param params Parameters (Optional) used to build/execute this use case.
     */
    fun execute(observer: DisposableCompletableObserver, params: Params) : Completable {
        val observable = this.buildUseCaseCompletableObservable(params)
        subscriptions.add(observable.subscribeWith(observer))
        return observable
    }

    /**
     * Dispose from current [CompositeDisposable].
     */
    fun dispose() = subscriptions.clear()
}