package com.dbeginc.jetruby.domain.entities

import android.os.Parcel
import android.os.Parcelable
import com.squareup.moshi.Json

/**
 * Created by darel on 11.07.17.
 * Pojo Representing Images qualities
 */
data class Images(@Json(name = "hidpi") val hidpi: String?,
             @Json(name = "normal") val normal: String?,
             @Json(name = "teaser") val teaser: String?) : Parcelable {

    private constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, p1: Int) {
        parcel.writeString(hidpi)
        parcel.writeString(normal)
        parcel.writeString(teaser)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<Images> {
        override fun createFromParcel(parcel: Parcel): Images = Images(parcel)
        override fun newArray(size: Int): Array<Images?> = arrayOfNulls(size)
    }
}