package com.dbeginc.jetruby.domain.usecases

import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subscribers.DisposableSubscriber

/**
 * Created by darel on 10.07.17.
 *
 * Abstract class for a Use Case (Interactor in terms of Clean Architecture).
 * This abstract class represents a execution unit for different use cases (this means any use case
 * in the application should implement this contract).
 *
 * By convention each UseCase implementation will return the result using a {@link DisposableSubscriber}
 * that will execute its job in a background thread and will post the result in the UI thread.
 */
abstract class UseCase<T, Params> {

    private val subscriptions = CompositeDisposable()

    /**
     * Builds an [Flowable] which will be used when executing the current [UseCase].
     */
    internal abstract fun buildUseCaseObservable(params: Params): Flowable<T>

    /**
     * Executes the current use case.

     * @param observer [DisposableSubscriber] which will be listening to the observable build
     * * by [.buildUseCaseObservable] ()} method.c
     * *
     * @param params Parameters (Optional) used to build/execute this use case.
     */
    fun execute(observer: DisposableSubscriber<T>, params: Params) : Flowable<T> {
        val observable = this.buildUseCaseObservable(params)
        subscriptions.add(observable.subscribeWith(observer))
        return observable
    }

    /**
     * Dispose from current [CompositeDisposable].
     */
    fun dispose() = subscriptions.clear()
}