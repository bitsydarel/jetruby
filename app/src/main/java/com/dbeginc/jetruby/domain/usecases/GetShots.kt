package com.dbeginc.jetruby.domain.usecases

import com.dbeginc.jetruby.data.ShotsRepository
import com.dbeginc.jetruby.domain.entities.AsyncParams
import com.dbeginc.jetruby.domain.entities.Shot
import io.reactivex.Flowable

/**
 * Created by darel on 11.07.17.
 * Get Shots Use Case
 */
class GetShots(private val shotsProvider: ShotsRepository) : UseCase<List<Shot>, AsyncParams<String, Unit>>() {


    override fun buildUseCaseObservable(params: AsyncParams<String, Unit>): Flowable<List<Shot>> {
        return shotsProvider.getShots(params.value)
    }
}