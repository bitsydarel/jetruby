package com.dbeginc.jetruby.domain.entities

import android.arch.persistence.room.*
import android.os.Parcel
import android.os.Parcelable

/**
 * Created by darel on 10.07.17.
 * Pojo Representing a Shot Object
 */
@Entity(tableName = "shots")
data class Shot(@PrimaryKey val id: Long,
                @ColumnInfo val title:String,
                @ColumnInfo var description: String?,
                @Embedded val images: Images,
                val animated: Boolean // Cant be ignored with @Ignore (Room Bug...)
) : Parcelable {

    private constructor(parcel: Parcel) : this(
            parcel.readLong(),
            parcel.readString(),
            parcel.readString(),
            parcel.readParcelable<Images>(Images::class.java.classLoader),
            parcel.readByte() != 0.toByte()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(title)
        parcel.writeString(description)
        parcel.writeParcelable(images, flags)
        parcel.writeByte(if (animated) 1 else 0)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<Shot> {
        override fun createFromParcel(parcel: Parcel): Shot {
            return Shot(parcel)
        }

        override fun newArray(size: Int): Array<Shot?> {
            return arrayOfNulls(size)
        }
    }
}