package com.dbeginc.jetruby.domain.entities

/**
 * Created by darel on 10.07.17.
 * Class that represent Async Use Case Parameter
 *
 */
data class AsyncParams<out V, out O>(val value: V, val observer: O)