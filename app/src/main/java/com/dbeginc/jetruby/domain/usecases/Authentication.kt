package com.dbeginc.jetruby.domain.usecases

import android.support.v4.app.FragmentActivity
import com.agilie.dribbblesdk.service.auth.AuthCredentials
import com.agilie.dribbblesdk.service.auth.DribbbleAuthHelper
import com.dbeginc.jetruby.data.authentification.IAuthService
import com.dbeginc.jetruby.domain.entities.AsyncParams
import io.reactivex.Completable

/**
 * Created by darel on 10.07.17.
 * Authentication Use Case
 */
class Authentication(val authService: IAuthService<AuthCredentials, Pair<FragmentActivity, DribbbleAuthHelper.AuthListener>>)
    : UseCaseCompletable<AsyncParams<AuthCredentials, Pair<FragmentActivity, DribbbleAuthHelper.AuthListener>>>() {

    override fun buildUseCaseCompletableObservable(params: AsyncParams<AuthCredentials, Pair<FragmentActivity, DribbbleAuthHelper.AuthListener>>): Completable {
        return Completable.fromAction {
            authService.login(params.value, params.observer)
        }
    }
}