package com.dbeginc.jetruby

import android.app.Application
import com.dbeginc.jetruby.di.components.DaggerJetRubyAppComponent
import com.dbeginc.jetruby.di.components.JetRubyAppComponent
import com.dbeginc.jetruby.di.modules.AppModule

/**
 * Created by darel on 10.07.17.
 * JetRuby Dribble App
 */
class JetRubyApp : Application() {
    companion object {
        @JvmStatic
        lateinit var graph: JetRubyAppComponent
    }

    override fun onCreate() {
        super.onCreate()

        graph = DaggerJetRubyAppComponent.builder()
                .appModule(AppModule(this))
                .build()
    }
}