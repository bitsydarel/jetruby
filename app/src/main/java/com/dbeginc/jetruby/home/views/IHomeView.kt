package com.dbeginc.jetruby.home.views

/**
 * Created by darel on 10.07.17.
 * Representation a home screen view
 */
interface IHomeView {
    /**
     * Setup Toolbar
     */
    fun setupToolbar()

    /**
     * Authenticate User
     */
    fun authenticate()

    /**
     * Show user timeline
     */
    fun showTimeLine()

    /**
     * Check if the first time user launch the app
     */
    fun isFirstRun() : Boolean
}