package com.dbeginc.jetruby.home.presenter

import com.dbeginc.jetruby.base.IPresenter
import com.dbeginc.jetruby.home.views.IHomeView

/**
 * Created by darel on 10.07.17.
 * MainView Presenter
 */
class HomePresenter : IPresenter<IHomeView> {
    lateinit var view: IHomeView

    override fun bind(newView: IHomeView) {
        view = newView
        view.setupToolbar()
        if (view.isFirstRun()) view.authenticate()
        else view.showTimeLine()
    }

    override fun unbind() {
        // Nothing to release, so it's empty
    }
}