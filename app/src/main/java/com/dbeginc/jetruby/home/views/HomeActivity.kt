package com.dbeginc.jetruby.home.views

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import com.dbeginc.jetruby.JetRubyApp
import com.dbeginc.jetruby.R
import com.dbeginc.jetruby.authentication.view.AuthenticationFragment
import com.dbeginc.jetruby.base.BaseActivity
import com.dbeginc.jetruby.databinding.ActivityMainBinding
import com.dbeginc.jetruby.di.qualifiers.AuthenticationEvent
import com.dbeginc.jetruby.home.presenter.HomePresenter
import com.dbeginc.jetruby.timeline.views.TimeLineFragment
import com.dbeginc.jetruby.utils.ConstantHolder
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

/**
 * Home Activity, impl of the HomeView
 */
class HomeActivity : BaseActivity(), IHomeView {

    @Inject lateinit var presenter: HomePresenter
    @Inject @field:AuthenticationEvent
    lateinit var authEvent: PublishSubject<Boolean>

    lateinit var binding: ActivityMainBinding
    private val authView = AuthenticationFragment()
    private lateinit var timeline : TimeLineFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        JetRubyApp.graph.inject(this)
        timeline = supportFragmentManager.findFragmentByTag(ConstantHolder.TIMELINE_FRAGMENT) as TimeLineFragment? ?: TimeLineFragment()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        presenter.bind(this)
        authEvent.subscribe(AuthObserver())
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        if (timeline.isAdded) {
            supportFragmentManager.putFragment(outState, ConstantHolder.TIMELINE_FRAGMENT, timeline)
        }
    }

    override fun onStop() {
        presenter.unbind()
        super.onStop()
    }

    override fun isFirstRun(): Boolean = sharedPref.getBoolean(ConstantHolder.IS_FIRST_RUN,true)

    override fun setupToolbar() {
        setSupportActionBar(binding.toolbar?.configToolbar)
        supportActionBar?.setTitle(R.string.app_name)
        supportActionBar?.setIcon(R.mipmap.ic_launcher)
    }

    override fun showTimeLine() {
        navigator.startFragment(supportFragmentManager, R.id.fragmentContainer, timeline, tag = ConstantHolder.TIMELINE_FRAGMENT)
    }

    override fun authenticate() {
        navigator.startFragment(supportFragmentManager, R.id.fragmentContainer, authView)
    }

    inner class AuthObserver: Observer<Boolean>  {
        lateinit var disposable: Disposable
        override fun onSubscribe(d: Disposable) { disposable = d }
        override fun onNext(result: Boolean) { if (result) showTimeLine() }
        override fun onError(t: Throwable) = Snackbar.make(binding.mainContainer, t.localizedMessage, Snackbar.LENGTH_LONG).show()
        override fun onComplete() = disposable.dispose()
    }
}
