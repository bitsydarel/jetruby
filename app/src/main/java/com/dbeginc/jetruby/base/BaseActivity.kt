package com.dbeginc.jetruby.base

import android.content.SharedPreferences
import android.content.res.Resources
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.dbeginc.jetruby.JetRubyApp
import com.dbeginc.jetruby.utils.Navigator
import javax.inject.Inject

/**
 * Created by darel on 10.07.17.
 * Base Activity
 *
 */
abstract class BaseActivity : AppCompatActivity() {

    @Inject protected lateinit var navigator: Navigator
    @Inject protected lateinit var resource: Resources
    @Inject protected lateinit var sharedPref : SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        JetRubyApp.graph
                .inject(this)
    }
}