package com.dbeginc.jetruby.base

/**
 * Created by darel on 10.07.17.
 * Base Presenter representation
 */
interface IPresenter<in V> {

    /**
     * Bind the presenter to his view
     * setup the view is default state
     * @param view to be bind
     */
    fun bind(newView: V)

    /**
     * UnBind the presenter to his view
     * release all resource and data related to the view
     */
    fun unbind()
}