package com.dbeginc.jetruby.base

import android.content.SharedPreferences
import android.content.res.Resources
import android.os.Bundle
import android.support.v4.app.Fragment
import com.dbeginc.jetruby.JetRubyApp
import com.dbeginc.jetruby.utils.Navigator
import javax.inject.Inject

/**
 * Created by darel on 11.07.17.
 * Base Fragment
 */
open class BaseFragment : Fragment() {

    @Inject protected lateinit var navigator: Navigator
    @Inject protected lateinit var resource: Resources
    @Inject lateinit var sharedPref : SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        JetRubyApp.graph
                .inject(this)
    }
}