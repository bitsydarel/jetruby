package com.dbeginc.jetruby.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.dbeginc.jetruby.data.locale.LocalShotProvider
import com.dbeginc.jetruby.domain.entities.Shot

/**
 * Created by darel on 11.07.17.
 * Jet Ruby App Database
 */
@Database(entities = arrayOf(Shot::class), version = 1)
abstract class AppDatabase : RoomDatabase(){

    abstract fun getShotsTable() : LocalShotProvider
}