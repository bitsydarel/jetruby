package com.dbeginc.jetruby.data

import android.os.Build
import android.text.Html
import com.dbeginc.jetruby.data.locale.LocalShotProvider
import com.dbeginc.jetruby.domain.entities.Shot
import io.reactivex.Flowable
import io.reactivex.Scheduler
import org.threeten.bp.LocalTime

/**
 * Created by darel on 11.07.17.
 * Shots Repository
 */
class ShotsRepository(private val localProvider: IShotProvider<String, Flowable<List<Shot>>>,
                      private val remoteProvider: IShotProvider<String, Flowable<List<Shot>>>,
                      val ioThread: Scheduler) {

    private var lastUpdate = 0

    fun getShots(token: String): Flowable<List<Shot>> {
        //Check if 2 minutes pass since we did the update then check the api
        //i would put between 5 to 10 but since it's only a test app
        return if (lastUpdate != 0 && (LocalTime.now().minute  - lastUpdate) > 1) {

            remoteProvider.getShots(token)
                    .subscribeOn(ioThread)
                    .flatMap { Flowable.fromIterable(it) }
                    .filter { !it.animated }
                    .filter { !it.description.isNullOrEmpty() }
                    .flatMap {
                        it.description = it.description?.removeHtml()
                        Flowable.just(it)
                    }
                    .take(50)
                    .toList()
                    .toFlowable()
                    .doOnNext {
                        lastUpdate = LocalTime.now().minute
                        localProvider as LocalShotProvider
                        localProvider.updateShots(it)
                    }
        } else {
            var databaseEmpty = false

            localProvider.getShots("id")
                    .subscribeOn(ioThread)
                    .flatMap {
                        if (it.isEmpty()) {
                            //Means first start of the app
                            databaseEmpty = true
                            remoteProvider.getShots(token)
                                    .subscribeOn(ioThread)
                                    .flatMap { Flowable.fromIterable(it) }
                                    .filter { !it.animated }
                                    .filter { !it.description.isNullOrEmpty()  }
                                    .flatMap {
                                        it.description = it.description?.removeHtml()
                                        Flowable.just(it)
                                    }
                                    .take(50)
                                    .toList()
                                    .toFlowable()
                        }
                        else Flowable.just(it)
                    }
                    .doOnNext {
                        if (databaseEmpty) {
                            localProvider as LocalShotProvider
                            localProvider.insertShots(it)
                            lastUpdate = LocalTime.now().minute

                        } else if (lastUpdate == 0) lastUpdate = LocalTime.now().minute
                    }
        }
    }

    fun String.removeHtml() : String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY).toString()
        else Html.fromHtml(this).toString()
    }
}