package com.dbeginc.jetruby.data.locale

import android.arch.persistence.room.*
import com.dbeginc.jetruby.data.IShotProvider
import com.dbeginc.jetruby.domain.entities.Shot
import io.reactivex.Flowable

/**
 * Created by darel on 11.07.17.
 * Locale Shot Provider
 */
@Dao
interface LocalShotProvider : IShotProvider<String, @kotlin.jvm.JvmSuppressWildcards Flowable<List<Shot>>> {

    @Query(value = "SELECT * FROM shots ORDER BY :param ASC")
    override fun getShots(param: String): Flowable<List<Shot>>

    @Query(value = "SELECT * FROM shots WHERE id = :id")
    fun getShotById(id: Int): Flowable<List<Shot>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertShots(listOfShots: List<Shot>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateShots(listOfShots: List<Shot>)
}