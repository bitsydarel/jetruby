package com.dbeginc.jetruby.data

/**
 * Created by darel on 11.07.17.
 * Representation of a Shot provider
 */
interface IShotProvider<in P, out R> {
    fun getShots(param: P) : R
}