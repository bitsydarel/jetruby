package com.dbeginc.jetruby.data.authentification

import android.support.v4.app.FragmentActivity
import com.agilie.dribbblesdk.service.auth.AuthCredentials
import com.agilie.dribbblesdk.service.auth.DribbbleAuthHelper

/**
 * Created by darel on 10.07.17.
 * Dribble Api Auth Listener
 */
class DribbleAuthService : IAuthService<AuthCredentials, Pair<FragmentActivity, DribbbleAuthHelper.AuthListener>> {

    override fun login(credentials: AuthCredentials, observer: Pair<FragmentActivity, DribbbleAuthHelper.AuthListener>) {
        DribbbleAuthHelper.startOauthDialog(observer.first, credentials, observer.second)
    }
}