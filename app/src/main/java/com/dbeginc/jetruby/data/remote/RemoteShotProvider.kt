package com.dbeginc.jetruby.data.remote

import com.dbeginc.jetruby.data.IShotProvider
import com.dbeginc.jetruby.domain.entities.Shot
import io.reactivex.Flowable
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by darel on 11.07.17.
 *
 */
class RemoteShotProvider(retrofit: Retrofit) : IShotProvider<String, Flowable<List<Shot>>> {

    private val shotService : ShotService = retrofit.create(ShotService::class.java)

    override fun getShots(param: String): Flowable<List<Shot>> =
            shotService.getShots(1, 500, "debuts", "recent", param)

    fun getShot(id: Long, token: String) : Flowable<Shot> = shotService.getShot(id, token)
}

interface ShotService {
    @GET("shots")
    fun getShots(@Query("page") page: Int, @Query("per_page") perPage: Int, @Query("list") types: String, @Query("sort") sortBy: String, @Query("access_token") token: String)
            : Flowable<List<Shot>>

    @GET("shots")
    fun getShot(@Query("id") id: Long, @Query("access_token") token: String) : Flowable<Shot>
}