package com.dbeginc.jetruby.data.authentification

/**
 * Created by darel on 10.07.17.
 * Authentication Provider
 */
interface IAuthService<in Credentials, in Observer> {

    fun login(credentials: Credentials, observer: Observer)
}