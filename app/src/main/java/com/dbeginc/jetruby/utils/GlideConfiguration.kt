package com.dbeginc.jetruby.utils

import android.content.Context
import com.bumptech.glide.Glide
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.Registry
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory
import com.bumptech.glide.module.AppGlideModule
import com.bumptech.glide.request.RequestOptions
import com.dbeginc.jetruby.JetRubyApp
import okhttp3.OkHttpClient
import javax.inject.Inject
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader
import com.bumptech.glide.load.model.GlideUrl
import com.dbeginc.jetruby.di.qualifiers.ImageCache
import com.dbeginc.jetruby.di.qualifiers.ImageDownloader
import java.io.InputStream


/**
 * Created by darel on 10.07.17.
 * Glide Specific configuration
 */
@GlideModule
class GlideConfiguration : AppGlideModule() {

    @Inject @field:ImageCache
    lateinit var diskCache: InternalCacheDiskCacheFactory

    @Inject @field:ImageDownloader
    lateinit var client: OkHttpClient

    init {
        JetRubyApp.graph.inject(this)
    }

    override fun applyOptions(context: Context?, builder: GlideBuilder?) {
        super.applyOptions(context, builder)
        builder?.setDiskCache(diskCache)
                ?.setDefaultRequestOptions(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
                ?.setDefaultRequestOptions(RequestOptions.formatOf(DecodeFormat.PREFER_ARGB_8888))
    }

    override fun isManifestParsingEnabled(): Boolean = false

    override fun registerComponents(context: Context?, glide: Glide?, registry: Registry?) {
        super.registerComponents(context, glide, registry)
        registry?.replace(GlideUrl::class.java, InputStream::class.java, OkHttpUrlLoader.Factory(client))
    }
}