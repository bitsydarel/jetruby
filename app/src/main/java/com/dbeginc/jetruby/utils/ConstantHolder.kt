package com.dbeginc.jetruby.utils


/**
 * Created by darel on 10.07.17.
 * Constant Holder class
 */
object ConstantHolder {
    val APP_CACHE_NAME = "app_cache_dir"
    val GlIDE_CACHE_NAME = "glide_cache"
    val CACHE_SIZE: Long = 50 * 1024 * 1024
    val  SHARED_PREF_NAME = "jetruby_test_pref"
    val  AUTH_TOKEN = "auth_token"
    val  IS_FIRST_RUN = "is_first_run"
    val ShOTS_URL="https://api.dribbble.com/v1/"
    val SHOTS_KEY="SHOTS"
    val TIMELINE_FRAGMENT = "TIMELINE_FRAGMENT"
}