package com.dbeginc.jetruby.utils

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.dbeginc.jetruby.domain.entities.Images


/**
 * Created by darel on 11.07.17.
 * Files that regroup binding adapters
 * For Layouts @see https://developer.android.com/topic/libraries/data-binding/index.html
 */

@BindingAdapter("setImageUrl")
fun setImage(imageView: ImageView, images: Images) {

    val url : String = if (images.hidpi.canBeUsed()) images.hidpi as String
                        else if (images.normal.canBeUsed()) images.normal as String
                        else if (images.teaser.canBeUsed()) images.teaser as String
                        else ""

    if (!url.isEmpty()) {
        Glide.with(imageView.context)
                .load(url)
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                .apply(RequestOptions.skipMemoryCacheOf(true))
                .into(imageView)
    }
}

/**
 * Check if not null and not empty
 * @return boolean if string can be used or not
 */
fun String?.canBeUsed() : Boolean = this != null && !this.isEmpty()
