package com.dbeginc.jetruby.utils

import android.app.Activity
import android.content.Intent
import android.support.annotation.AnimRes
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager

/**
 * Created by darel on 10.07.17.
 * App Navigator
 */
class Navigator {

    fun startActivity(activity: Activity, intent: Intent) = activity.startActivity(intent)

    fun startFragment(fragmentManager: FragmentManager, @IdRes container: Int, fragment: Fragment,
                      @AnimRes enteringAnimation: Int = 0, @AnimRes exitingAnimation: Int = 0,
                      tag: String = "") {

        if (!fragment.isAdded) {
            fragmentManager.beginTransaction()
                    .setCustomAnimations(enteringAnimation, exitingAnimation)
                    .replace(container, fragment, tag)
                    .commitAllowingStateLoss()
        }
    }
}